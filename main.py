import random

class GameState:
    def __init__(self):
        # Dictonary of lists containing space info
        # boardLocation:  name, cost, type, color, owner
        self.spaces = {  
            0: ["go", 200, "INCOME", "NA", "none"],
            1: ["Mediterranean Avenue", 60, "PROPERTY", "brown", "none"],
            2: ["Community Chest", 0, "CHEST", "NA", "none"],
            3: ["Baltic Avenue", 60, "PROPERTY", "brown" , "none"],
            4: ["Income Tax", 200, "LIABILITY", "NA", "none"],
            5: ["Reading Railroad", 200, "PROPERTY", "rail", "none"],
            6: ["Oriental Avenue", 100, "PROPERTY", "teal", "none"],
            7: ["Chance", 0, "CHANCE", "NA", "none"],
            8: ["Vermont Avenue", 100, "PROPERTY", "teal", "none"],
            9: ["Connecticut Avenue", 120, "PROPERTY", "teal", "none"],
            10: ["Jail Visit", 0, "PASS", "NA", "none"],
            11: ["St. Charles Place", 140, "PROPERTY", "pink", "none"],
            12: ["Electric Company", 150, "PROPERTY", "util", "none"],
            13: ["States Avenue", 140, "PROPERTY", "pink", "none"],
            14: ["Virginia Avenue", 160, "PROPERTY", "pink", "none"],
            15: ["Pennsylvania Railroad", 200, "PROPERTY", "rail", "none"],
            16: ["St. James Place", 180, "PROPERTY", "orange", "none"],
            17: ["Community Chest", 0, "CHEST", "NA", "none"],
            18: ["Tennessee Avenue", 180, "PROPERTY", "orange", "none"],
            19: ["New York Avenue", 200, "PROPERTY", "orange", "none"],
            20: ["Free parking", 0, "PASS", "NA", "none"],
            21: ["Kentucky Avenue", 220, "PROPERTY", "red", "none"],
            22: ["Chance", 0, "CHANCE", "NA", "none"],
            23: ["Indiana Avenue", 220, "PROPERTY", "red", "none"],
            24: ["Illinois Avenue", 240, "PROPERTY", "red", "none"],
            25: ["B & O. Railroad", 200, "PROPERTY", "rail", "none"],
            26: ["Atlantic Avenue", 260, "PROPERTY", "yellow", "none"],
            27: ["Ventnor Avenue", 260, "PROPERTY", "yellow", "none"],
            28: ["Water Works", 150, "PROPERTY", "util", "none"],
            29: ["Marvin Gardens", 280, "PROPERTY", "yellow", "none"],
            30: ["Go to Jail", 0, "JAIL", "NA", "none"],
            31: ["Pacific Avenue", 300, "PROPERTY", "green", "none"],
            32: ["North Carolina Avenue", 300, "PROPERTY", "green", "none"],
            33: ["Community Chest", 0, "CHEST", "NA", "none"],
            34: ["Pennsylvania Avenue", 320, "PROPERTY", "green", "none"],
            35: ["Short Line", 200, "PROPERTY", "rail", "none"],
            36: ["Chance", 0, "CHANCE", "NA", "none"],
            37: ["Park Place", 350, "PROPERTY", "blue", "none"],
            38: ["Luxary Tax", 0, "LIABILITY", "NA", "none"],
            39: ["BoardWalk", 400, "PROPERTY", "blue", "none"]
        }

        # a handy lookup 
        self.lookup = {
            "name": 0,
            "cost": 1,
            "type": 2,
            "color": 3,
            "owner": 4
        }

    # function to add user 

    # function to see YOUR properties

    # function to see sets

    # funciton to view all monopoly properties 
    def viewProperties(self):
        

        # view ALL properties
        for i in self.spaces:
            
            if "PROPERTY" in self.spaces[i]:

                #print(self.spaces[i][0], self.spaces[i][1], self.spaces[i][3])
                print(self.spaces[i])


class Player:
    def __init__(self, name, bank, location):
        self.name = name
        self.bank = 1500
        self.location = 0 

    def myfunc(self):
        print("My name is", self.name)

    # get balance player.getBalance
    def getBalance(self):
        return self.bank

def rollDice():
    return(int(random.randrange(2,12)))


def mainMenu(player):

    print("======================================\nPlayer turn", player.name)
    print("current location", state.spaces[player.location], "\n")
    
    print(state.viewProperties())
    
    cont = True
    while cont:
        choice = input("""What would you like to do?
            r - Roll Dice
            b - View your bank account balance
            q - Quit game
                > """)
        
        # player rolls
        if choice == "r":
            rollOption(player)
            cont = False
        elif choice == "b":
            print("Your balance is: $",player.getBalance())
        elif choice == "q":
            quit()
    
    input("Press Enter to continue...")
    print(".\n.\n.\n.\n.\n")


def rollOption(player):
    
    # get a roll as int
    roll = rollDice()

    print("rolled", roll)

    # reset to 0 once looping around the board
    if player.location + roll > 39:
        player.location = (player.location + roll) - 40              
        print("new location", player.location,  state.spaces[player.location], "\n")
    else:
        player.location += roll
        print("new location", player.location,  state.spaces[player.location], "\n")
    


if __name__ == "__main__":
   
 
    # create and instance of class "Player", called p1
    # Will be used to keep track of p1 assets 
    p1 = Player("Nick", 1500, 0)

    p2 = Player("Jenna", 1500, 0)

    # Create instance of class "GameState" to store game state info such as:
    # - who owns what
    # - how much a property costs
    state = GameState()

    # a lookup for spaces (makes life a bit easier)


    rnd = 1
    
    
    while True: 

        print("\nRound", rnd, "\n")
        # reset
        

        # initiate options for user 1
        mainMenu(p1)

        # initiate options for user2
        mainMenu(p2)
        

        #that = input("helo")
        

        #mainMenu(p1)


        #print("Player turn", p2.name)
        #mainMenu(p2)
        
        


        

        # change age to 28
        #p1.age = 28

        #print(p1.age)

        #p1.myfunc()

        #rollDice()
        





    #for i in spaces:
    #    print(spaces[i][lookup["owner"]])
        rnd += 1